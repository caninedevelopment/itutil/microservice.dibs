﻿function checkout() {
    console.log("test");

    var checkoutOptions =
    {
        checkoutKey: "live- checkout - key - 00000000000000000000000009000000", //[Required] Test or Live GUID with dashes
        paymentId: "8b464458f2524bc39fe5d31deb8bedc1", //[required] GUID without dashes
        partnerMerchantNumber: "123456789", //[optional] Number
        containerId : "dibs-complete-checkout", //[optional] defaultValue: dibs-checkout-content
        language: "en-GB",//[optional] defaultValue: en-GB
        theme:
        { // [optional] - will change defaults in the checkout
            textColor: "blue", // any valid css color
            linkColor: "#bada55", // any valid css color
            panelTextColor: "rgb(125, 125, 125)", // any valid css color
            panelLinkColor: "#0094cf", // any valid css color
            primaryColor: "#0094cf", // any valid css color
            buttonRadius: "50px", // pixel or percentage value
            buttonTextColor: "#fff", // any valid css color
            backgroundColor: "#eee", // any valid css color
            panelColor: "#fff", // any valid css color
            outlineColor: "#444", // any valid css color
            primaryOutlineColor: "#444", // any valid css color   
        }

    };
    var checkout = new Dibs.Checkout(checkoutOptions);

//this is the event that the merchant should listen to redirect to the “payment-is-ok” page

    checkout.on('payment-completed', function (response) {
    /*
    Response:
                   paymentId: string (GUID without dashes)
    */
    window.location = '/PaymentSuccessful';
    });
}