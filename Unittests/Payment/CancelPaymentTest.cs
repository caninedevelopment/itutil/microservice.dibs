﻿using Monosoft.Service.Dibs;
using Monosoft.Service.Dibs.Commands.Setting;
using Monosoft.Service.Dibs.DTO;
using Monosoft.Service.Dibs.DTO.Payment;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Unittests.Payment
{
    [TestFixture]
    class CancelPaymentTest
    {
        private Logic logic;

        [SetUp]
        public void Setup()
        {
            this.logic = new Logic("dibs");
            this.logic.SetSettings(new Monosoft.Service.Dibs.Database.Settings(true, "e56aee164e38429d9d5a673e7672c1dc", "https://fakeurl.com/terms"));
        }

        [Test]
        public void CancelPayment()
        {
            Monosoft.Service.Dibs.DTO.Payment.CancelRequest cancelRequest = new CancelRequest()
            {
                amount = 400,
                orderItems = new Item[]
                {
                    new Item()
                    {
                        Reference = "ref001",
                        Name = "testSub",
                        Quantity = 1,
                        Unit = "Zealots",
                        UnitPrice = 400,
                        TaxRate = 2500,
                        NetTotalAmount = 400,
                    },
                },
                paymentId = "010600005ede342eabcd1b1b7cc7bcf5",
            };


            //My single payments feb
          
            var result = this.logic.CancelSubscription(cancelRequest);

       

            Assert.IsTrue(result.didSucceed);


        }

    }
}
