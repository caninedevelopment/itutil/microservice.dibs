﻿using Monosoft.Service.Dibs;
using Monosoft.Service.Dibs.Commands.Setting;
using Monosoft.Service.Dibs.DTO;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Unittests.Payment
{
    [TestFixture]
    class ChargePaymentTest
    {
        private Logic logic;

        [SetUp]
        public void Setup()
        {
            this.logic = new Logic("dibs");
            this.logic.SetSettings(new Monosoft.Service.Dibs.Database.Settings(true, "e56aee164e38429d9d5a673e7672c1dc", "https://fakeurl.com/terms"));
        }

        [Test]
        public void WrongOrderReference()
        {
            Assert.That(
            () => this.logic.SinglePaymentCharge(new Monosoft.Service.Dibs.DTO.Payment.SingleChargeCreationInfo() {orderReference = "Charge with wrong info", orderItems = new Item[]
                {
                    new Item()
                    {
                        Reference = "ref001",
                        Name = "testSub",
                        Quantity = 1,
                        Unit = "Zealots",
                        UnitPrice = 400,
                        TaxRate = 2500,
                        NetTotalAmount = 400,
                    },
                },
            }),
            Throws.Exception.TypeOf<NullReferenceException>()
            );
        }

        [Test]
        public void EmptyOrderRerefernece()
        {
            Assert.That(
          () => this.logic.SinglePaymentCharge(new Monosoft.Service.Dibs.DTO.Payment.SingleChargeCreationInfo()
          {
              orderReference = null,
              orderItems = new Item[]
              {
                    new Item()
                    {
                        Reference = "ref001",
                        Name = "testSub",
                        Quantity = 1,
                        Unit = "Zealots",
                        UnitPrice = 400,
                        TaxRate = 2500,
                        NetTotalAmount = 400,
                    },
              },
          }),
          Throws.Exception.TypeOf<NullReferenceException>()
          );

        }
    }
}
