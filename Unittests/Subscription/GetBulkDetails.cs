﻿using Monosoft.Service.Dibs;
using Monosoft.Service.Dibs.Commands.Setting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Unittests.Subscription
{
    class GetBulkDetails
    {
        private Logic logic;

        [SetUp]
        public void Setup()
        {
            this.logic = new Logic("dibs");
            this.logic.SetSettings(new Monosoft.Service.Dibs.Database.Settings(true, "e56aee164e38429d9d5a673e7672c1dc", "https://fakeurl.com/terms"));
        }

        [Test]
        public void WrongExternalId()
        {
            Assert.That(
            () => this.logic.GetBulkDetailsFull(new Monosoft.Service.Dibs.DTO.Charge.Bulk.BulkExternalId { id = "Wrong ordereference" }),
            Throws.Exception.TypeOf<NullReferenceException>()
            );
        }

        [Test]
        public void EmptyExternalID()
        {
            Assert.That(
          () => this.logic.GetBulkDetailsFull(new Monosoft.Service.Dibs.DTO.Charge.Bulk.BulkExternalId { id = null }),
          Throws.Exception.TypeOf<NullReferenceException>()
          );

        }
    }
}
