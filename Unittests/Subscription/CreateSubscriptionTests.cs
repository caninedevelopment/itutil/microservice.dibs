﻿using System;
using System.Collections.Generic;
using System.Text;
using Monosoft.Service.Dibs;
using Monosoft.Service.Dibs.Commands.Setting;
using Monosoft.Service.Dibs.Commands.Subscription;
using Monosoft.Service.Dibs.DTO;
using Monosoft.Service.Dibs.DTO.Payment;
using NUnit.Framework;

namespace Unittests.Subscription
{


    [TestFixture]
    public class CreateSubscriptionTests
    {
        private Logic logic;

        [SetUp]
        public void Setup()
        {
            this.logic = new Logic("dibs");
            this.logic.SetSettings(new Monosoft.Service.Dibs.Database.Settings(true, "e56aee164e38429d9d5a673e7672c1dc", "https://fakeurl.com/terms"));
        }

        [Test]
        public void CreateSubScription()
        {

            SubScriptionCreationInfo subScriptionCreationInfo = new SubScriptionCreationInfo()
            {
                consumerType = null,
                currency = "DKK",
                endDate = DateTime.Parse("2020-07-18T00:00:00+00:00"),
                immediateCharge = false,
                interval = 0,
                items = new Item[]
                {
                    new Item()
                    {
                        Reference = "ref001",
                        Name = "testSub",
                        Quantity = 1,
                        Unit = "Zealots",
                        UnitPrice = 400,
                        TaxRate = 2500,
                        NetTotalAmount = 400,
                    },
                    new Item()
                    {
                        Reference = "ref002",
                        Name = "testSub",
                        Quantity = 1,
                        Unit = "Zealots",
                        UnitPrice = 400,
                        TaxRate = 2500,
                        NetTotalAmount = 400,
                    },
                },
                merchantHandlesShippingCost = false,
                merchantNumber = null,
                orderReference = "My zealots belong to chili" + Guid.NewGuid(),
                shippingCountries = new Country[]
                {

                },
                notifications = new Monosoft.Service.Dibs.DTO.Internal.Notifications()
                {

                    webhooks = new Monosoft.Service.Dibs.DTO.Internal.Webhook[]
                 {
                     new Monosoft.Service.Dibs.DTO.Internal.Webhook()
                     {
                            authorization = Guid.NewGuid().ToString() /*TODO!!!*/,
                            eventName = "payment.checkout.completed",
                            url = "https://madfaerd.monosoft.dk/madfaerd_subscription/v1/onCheckoutCompleted"//SEE: https://tech.dibspayment.com/easy/api/paymentapi
                     }
                 }
                },
                url = "https://madfaerd.monosoft.dk?pram1=1&pram2=2&pram3=3",
            };
            var result =this.logic.CreateSubscription(subScriptionCreationInfo);


            Assert.IsNotNull(result.paymentId);
        }

        [Test]
        public void CreateDublicateSubscription()
        {
            SubScriptionCreationInfo subScriptionCreationInfo = new SubScriptionCreationInfo()
            {
                consumerType = null,
                currency = "DKK",
                endDate = DateTime.Parse("2020-07-18T00:00:00+00:00"),
                immediateCharge = false,
                interval = 0,
                items = new Item[]
                {
                    new Item()
                    {
                        Reference = "ref001",
                        Name = "testSub",
                        Quantity = 1,
                        Unit = "Zealots",
                        UnitPrice = 400,
                        TaxRate = 2500,
                        NetTotalAmount = 400,
                    },
                },
                merchantHandlesShippingCost = false,
                merchantNumber = null,
                orderReference = "My zealots belong to chili" + Guid.NewGuid(),
                shippingCountries = new Country[]
                {

                },
                url = "https://checkout.dibspayment.eu?pram1=1&pram2=2&pram3=3",
            };
            var result = this.logic.CreateSubscription(subScriptionCreationInfo);
            var exception = Assert.Throws<Exception>(() => this.logic.CreateSubscription(subScriptionCreationInfo));

            Assert.That(exception.Message, Is.EqualTo("orderreference already exist, please use an unique."));
        }

  

    }
}
