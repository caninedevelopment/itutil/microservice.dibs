﻿using Monosoft.Service.Dibs;
using Monosoft.Service.Dibs.Commands.Setting;
using Monosoft.Service.Dibs.DTO;
using Monosoft.Service.Dibs.DTO.Payment;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Unittests.Subscription
{
    [TestFixture]
    class GetSubscriptionDetailsTests
    {
        private Logic logic;

        [SetUp]
        public void Setup()
        {
            this.logic = new Logic("dibs");
            this.logic.SetSettings(new Monosoft.Service.Dibs.Database.Settings(true, "e56aee164e38429d9d5a673e7672c1dc", "https://fakeurl.com/terms"));
        }

        [Test]
        public void GetSinglePaymentDetails()
        {
            //My single payments feb
            SubScriptionCreationInfo subScriptionCreationInfo = new SubScriptionCreationInfo()
            {
                consumerType = null,
                currency = "DKK",
                endDate = DateTime.Parse("2020-07-18T00:00:00+00:00"),
                immediateCharge = false,
                interval = 0,
                items = new Item[]
              {
                    new Item()
                    {
                        Reference = "ref001",
                        Name = "testSub",
                        Quantity = 1,
                        Unit = "Zealots",
                        UnitPrice = 400,
                        TaxRate = 2500,
                        NetTotalAmount = 400,
                    },
              },
                merchantHandlesShippingCost = false,
                merchantNumber = null,
                orderReference = "My zealots belong to chili" + Guid.NewGuid(),
                shippingCountries = new Country[]
              {

              },
                url = "https://checkout.dibspayment.eu?pram1=1&pram2=2&pram3=3",
            };
            var result = this.logic.CreateSubscription(subScriptionCreationInfo);

            var paymentID = this.logic.GetSubscriptionPaymentDetails(new OrderReference()
            {
                orderReference = subScriptionCreationInfo.orderReference,
            });

            Assert.IsNotNull(paymentID.paymentId);


        }

        [Test]
        public void GetWrongPaymentDetails()
        {
            Assert.That(
                () => this.logic.GetSubscriptionPaymentDetails(new OrderReference()
                {
                    orderReference = "hula bulla",
                }),
                Throws.Exception.TypeOf<NullReferenceException>()
            );
        }
    }
}
