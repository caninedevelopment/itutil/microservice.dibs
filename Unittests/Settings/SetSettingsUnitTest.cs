﻿using System;
using System.Collections.Generic;
using System.Text;
using Monosoft.Service.Dibs;
using Monosoft.Service.Dibs.Commands.Setting;
using NUnit.Framework;

namespace Unittests.Settings
{

     [TestFixture]
     public class SetSettingsUnitTest
    {
        private Logic logic;

        [SetUp]
        public void Setup()
        {
            this.logic = new Logic("dibs");
        }

        [Test]
        public void SetSettingsTest()
        {
            this.logic.SetSettings(new Monosoft.Service.Dibs.Database.Settings(true, "e56aee164e38429d9d5a673e7672c1dc", "https://fakeurl.com/terms"));
         
        }
    }
}
