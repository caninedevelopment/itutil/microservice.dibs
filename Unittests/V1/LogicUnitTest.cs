﻿//// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
//// Copyright 2018 Monosoft Holding ApS
////
//// Licensed under the Apache License, Version 2.0 (the "License");
//// you may not use this file except in compliance with the License.
//// You may obtain a copy of the License at
////
////    http://www.apache.org/licenses/LICENSE-2.0
////
//// Unless required by applicable law or agreed to in writing, software
//// distributed under the License is distributed on an "AS IS" BASIS,
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//// See the License for the specific language governing permissions and
//// limitations under the License.
//// </copyright>

//namespace Unittests
//{
//    using ITUtil.Common.DTO;
//    using ITUtil.Common.Utils;
//    using Monosoft.Service.Dibs.V1;
//    using Monosoft.Service.Dibs.V1.DTO;
//    using Monosoft.Service.Dibs.V1.DTO.Charge.Bulk;
//    using Monosoft.Service.Dibs.V1.DTO.Charge.Payment;
//    using Monosoft.Service.Dibs.V1.DTO.Database;
//    using Monosoft.Service.Dibs.V1.DTO.Internal;
//    using Monosoft.Service.Dibs.V1.DTO.Payment;
//    using NUnit.Framework;
//    using System;
//    using Item = Monosoft.Service.Dibs.V1.DTO.Item;

//    /// <summary>
//    /// Contain all Unit tests for Dibs.
//    /// </summary>
//    [TestFixture]
//    public class LogicUnitTest
//    {

//        /// <summary>
//        /// Test if we can set the mechant id.
//        /// </summary>
//        [Test]
//        public void SetSettings()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new Settings(true, "e56aee164e38429d9d5a673e7672c1dc", "https://fakeurl.com/terms"));
//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new PaymentController();
//            controller.SetSettings(messageWrapper);
//        }

//        /// <summary>
//        /// Test creation of the subscription and containing data.
//        /// </summary>
//        [Test]
//        public void CreatePayment()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new PaymentCreationInfo()
//            {
//                consumerType = null,
//                currency = "DKK",
//                items = new Item[]
//                {
//                    new Item()
//                    {
//                        reference = "ref001",
//                        name = "testSub",
//                        quantity = 1,
//                        unit = "Zealots",
//                        unitPrice = 400,
//                        taxRate = 2500,
//                        netTotalAmount = 400,
//                    },
//                },
//                merchantHandlesShippingCost = false,
//                merchantNumber = null,
//                orderReference = "My zealots belong to chili" + Guid.NewGuid(),
//                shippingCountries = new Country[]
//                {

//                },
//                url = "https://checkout.dibspayment.eu?pram1=1&pram2=2&pram3=3",
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new PaymentController();
//            var result = controller.CreatePayment(messageWrapper);
//            var paymentId = MessageDataHelper.FromMessageData<PaymentSystem>(result.data);

//            Assert.IsTrue(result.success);
//            Assert.IsNotNull(paymentId.paymentId);
//            Assert.IsNotEmpty(paymentId.paymentId);
//        }

//        [Test]
//        public void GetSinglePaymentDetails()
//        {
//            //My single payments feb

//            var messageData = MessageDataHelper.ToMessageData(new OrderReference()
//            {
//                orderReference = "My single payments dec",
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new PaymentController();
//            var result = controller.GetPaymentDetails(messageWrapper);
//            var paymentId = MessageDataHelper.FromMessageData<PaymentSingle>(result.data);
//            Assert.IsNotNull(paymentId.paymentId);
//            Assert.IsNotEmpty(paymentId.paymentId);

//        }

//        [Test]
//        public void ChargeSinglePayment()
//        {
//            //My single payments feb

//            var messageData = MessageDataHelper.ToMessageData(new SingleChargeCreationInfo()
//            {
//                orderReference = "My single payments dec",
//                orderItems = new Item[]
//                {
//                    new Item()
//                    {
//                        reference = "ref001",
//                        name = "testSub",
//                        quantity = 1,
//                        unit = "Zealots",
//                        unitPrice = 400,
//                        taxRate = 2500,
//                        netTotalAmount = 400,
//                    },
//                },
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new PaymentController();
//            var result = controller.ChargePayment(messageWrapper);
//            var paymentId = MessageDataHelper.FromMessageData<ChargeInfo>(result.data);
//            Assert.IsNotNull(paymentId.chargeId);
//            Assert.IsNotEmpty(paymentId.chargeId);

//        }

//        /// <summary>
//        /// Test creation of the subscription and containing data.
//        /// </summary>
//        [Test]
//        public void CreateSubscription()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new SubScriptionCreationInfo()
//            {
//                consumerType = null,
//                currency = "DKK",
//                endDate = "2020-07-18T00:00:00+00:00",
//                immediateCharge = false,
//                interval = 0,
//                items = new Item[]
//                {
//                    new Item()
//                    {
//                        reference = "ref001",
//                        name = "testSub",
//                        quantity = 1,
//                        unit = "Zealots",
//                        unitPrice = 400,
//                        taxRate = 2500,
//                        netTotalAmount = 400,
//                    },
//                },
//                merchantHandlesShippingCost = false,
//                merchantNumber = null,
//                orderReference = "My zealots belong to chili" + Guid.NewGuid(),
//                shippingCountries = new Country[]
//                {

//                },
//                url = "https://checkout.dibspayment.eu?pram1=1&pram2=2&pram3=3",
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new SubScriptionController();
//            var result = controller.CreateSubscription(messageWrapper);
//            var paymentId = MessageDataHelper.FromMessageData<PaymentSystem>(result.data);

//            Assert.IsTrue(result.success);
//            Assert.IsNotNull(paymentId.paymentId);
//            Assert.IsNotEmpty(paymentId.paymentId);
//        }

//        [Test]
//        public void CreateDublicateSubscription()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new SubScriptionCreationInfo()
//            {
//                consumerType = null,
//                currency = "DKK",
//                endDate = "2020-07-18T00:00:00+00:00",
//                immediateCharge = false,
//                interval = 0,
//                items = new Item[]
//                {
//                    new Item()
//                    {
//                        reference = "ref001",
//                        name = "testSub",
//                        quantity = 1,
//                        unit = "Zealots",
//                        unitPrice = 400,
//                        taxRate = 2500,
//                        netTotalAmount = 400,
//                    },
//                },
//                merchantHandlesShippingCost = false,
//                merchantNumber = null,
//                orderReference = "My zealots belong to chili",
//                shippingCountries = new Country[]
//                {

//                },
//                url = "https://checkout.dibspayment.eu?pram1=1&pram2=2&pram3=3",
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new SubScriptionController();
//            var result = controller.CreateSubscription(messageWrapper);
//            var exception = Assert.Throws<Exception>(() => controller.CreateSubscription(messageWrapper));

//            Assert.That(exception.Message, Is.EqualTo("orderreference already exist, please use an unique."));
//        }

//        [Test]
//        public void GetPaymentDetails()
//        {
//            //00c300005e4a41dde9c15a393ce8ac36

//            var messageData = MessageDataHelper.ToMessageData(new OrderReference()
//            {
//                orderReference = "My zealotsz belong to chili Maj",
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new SubScriptionController();
//            var result = controller.GetPaymentDetails(messageWrapper);
//            var paymentId = MessageDataHelper.FromMessageData<Payment>(result.data);
//            Assert.IsNotNull(paymentId.paymentId);
//            Assert.IsNotEmpty(paymentId.paymentId);
//        }

//        /// <summary>
//        /// Return a subscription from the subscriptionID.
//        /// </summary>
//        [Test]
//        public void GetSubscriptionId()
//        {
//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = MessageDataHelper.ToMessageData(new OrderReference() { orderReference = "My zealotsz belong to chili Maj" });
//            //var paymentId = MessageDataHelper.ToMessageData<paymentSystem>(result.data);
//            var controller = new SubScriptionController();
//            var result = controller.GetSubscriptionId(messageWrapper);
//            var subscription = MessageDataHelper.FromMessageData<Monosoft.Service.Dibs.V1.DTO.Subscription>(result.data);

//            Assert.IsNotNull(subscription.id);
//            Assert.IsNotEmpty(subscription.id);
//            //Assert.Fail();

//            //"My zealots belong to chili"

//            //028e00005e4a637ce9c15a393ce8ad1c
//        }

//        /// <summary>
//        /// Create a bulk Charge to all the users that need charge right now to thier chosen payment.
//        /// </summary>
//        [Test]
//        public void BulkCharge()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new BulkChargeCreationInfo()
//            {
//                externalBulkChargeId = "fisk00" + Guid.NewGuid(),
//                subScriptions = new SubScriptionBulkCharge []
//                {
//                    new SubScriptionBulkCharge()
//                    {
//                        subscriptionId = "c246b9d6fe844cd4a9366bcbec1109d3",
//                        items = new Item[]
//                        {
//                            new Item()
//                            {
//                                reference = "ref001",
//                                name = "testSub",
//                                quantity = 1,
//                                unit = "Zealots",
//                                unitPrice = 400,
//                                taxRate = 2500,
//                                netTotalAmount = 400,
//                            },
//                        },
//                        order = new OrderDetails()
//                        {
//                            amount = 500,
//                            currency = "DKK",
//                            reference = "my bulk char 01",
//                        },
//                    },
//                },
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new SubScriptionController();
//            var result = controller.ChargeBulk(messageWrapper);
//            var bulk = MessageDataHelper.FromMessageData<BulkInfo>(result.data);

//            Assert.IsTrue(result.success);
//            Assert.IsNotNull(bulk.bulkChargeId);
//            Assert.IsNotEmpty(bulk.bulkChargeId);
//        }

//        [Test]
//        public void BulkChargeDublicates()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new BulkChargeCreationInfo()
//            {
//                externalBulkChargeId = "fisk00" + Guid.NewGuid(),
//                subScriptions = new SubScriptionBulkCharge[]
//                {
//                    new SubScriptionBulkCharge()
//                    {
//                        subscriptionId = "c246b9d6fe844cd4a9366bcbec1109d3",
//                        items = new Item[]
//                        {
//                            new Item()
//                            {
//                                reference = "ref001",
//                                name = "testSub",
//                                quantity = 1,
//                                unit = "Zealots",
//                                unitPrice = 400,
//                                taxRate = 2500,
//                                netTotalAmount = 400,
//                            },
//                        },
//                        order = new OrderDetails()
//                        {
//                            amount = 500,
//                            currency = "DKK",
//                            reference = "my bulk char 01",
//                        },
//                    },
//                    new SubScriptionBulkCharge()
//                    {
//                        subscriptionId = "095b389989104367933647517a515829",
//                        items = new Item[]
//                        {
//                            new Item()
//                            {
//                                reference = "ref001",
//                                name = "testSub",
//                                quantity = 1,
//                                unit = "Zealots",
//                                unitPrice = 400,
//                                taxRate = 2500,
//                                netTotalAmount = 400,
//                            },
//                        },
//                        order = new OrderDetails()
//                        {
//                            amount = 500,
//                            currency = "DKK",
//                            reference = "my bulk char 01",
//                        },
//                    },
//                    new SubScriptionBulkCharge()
//                    {
//                        subscriptionId = "f8c4c0fc33bf45b6a401c298dcfb67d9",
//                        items = new Item[]
//                        {
//                            new Item()
//                            {
//                                reference = "ref001",
//                                name = "testSub",
//                                quantity = 1,
//                                unit = "Zealots",
//                                unitPrice = 400,
//                                taxRate = 2500,
//                                netTotalAmount = 400,
//                            },
//                        },
//                        order = new OrderDetails()
//                        {
//                            amount = 500,
//                            currency = "DKK",
//                            reference = "my bulk char 01",
//                        },
//                    },
//                    new SubScriptionBulkCharge()
//                    {
//                        subscriptionId = "1ecbfc0247c64fc8ab9e0ee78365d4a9",
//                        items = new Item[]
//                        {
//                            new Item()
//                            {
//                                reference = "ref001",
//                                name = "testSub",
//                                quantity = 1,
//                                unit = "Zealots",
//                                unitPrice = 400,
//                                taxRate = 2500,
//                                netTotalAmount = 400,
//                            },
//                        },
//                        order = new OrderDetails()
//                        {
//                            amount = 500,
//                            currency = "DKK",
//                            reference = "my bulk char 01",
//                        },
//                    },
//                },
//            });

//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new SubScriptionController();
//            var result = controller.ChargeBulk(messageWrapper);
//            var bulk = MessageDataHelper.FromMessageData<BulkInfo>(result.data);

//            Assert.IsTrue(result.success);
//            Assert.IsNotNull(bulk.bulkChargeId);
//            Assert.IsNotEmpty(bulk.bulkChargeId);
//        }

//        [Test]
//        public void GetBulkDetailsFull()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new BulkExternalId()
//            {
//                id = "fisk0089f82c17-6dbd-4452-a923-e904a58daa19",
//            });
//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new SubScriptionController();
//            var result = controller.GetBulkDetails(messageWrapper);
//            var bulk = MessageDataHelper.FromMessageData<BulkDetails>(result.data);
//            Assert.IsNotNull(bulk);

//        }

//        [Test]
//        public void GetBulkDetailsPage()
//        {
//            var messageData = MessageDataHelper.ToMessageData(new BulkPageInfo()
//            {
//                externalId = "fisk0089f82c17-6dbd-4452-a923-e904a58daa19",
//                pageNumber = 1,
//                pageSize = 1,
//            });
//            var messageWrapper = new MessageWrapper();
//            messageWrapper.messageData = messageData;
//            var controller = new SubScriptionController();
//            var result = controller.GetBulkPage(messageWrapper);
//            var bulk = MessageDataHelper.FromMessageData<BulkDetails>(result.data);
//            Assert.IsNotNull(bulk);

//        }
//    }
//}