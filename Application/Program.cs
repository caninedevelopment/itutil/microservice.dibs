﻿
namespace Monosoft.Service.Dibs
{
    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new Monosoft.Service.Dibs.Commands.Subscription.Namespace(),
                new Monosoft.Service.Dibs.Commands.Payment.Namespace(),
                new Monosoft.Service.Dibs.Commands.Setting.Namespace(),
            });
        }
    }
}
