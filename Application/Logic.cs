﻿// <copyright file="Logic.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using Monosoft.Service.Dibs.DTO;
    using Monosoft.Service.Dibs.DTO.Charge.Bulk;
    using Monosoft.Service.Dibs.DTO.Charge.Payment;
    using Monosoft.Service.Dibs.Database;
    using Monosoft.Service.Dibs.DTO.Internal;
    using Monosoft.Service.Dibs.DTO.Payment;
    using Newtonsoft.Json;
    using Renci.SshNet.Messages.Authentication;

    /// <summary>
    /// Is operating all dibs functionality.
    /// </summary>
    public class Logic : IDisposable
    {
        private const string SubscriptionUrl = ".dibspayment.eu/v1/subscriptions/";
        private const string PaymentUrl = ".dibspayment.eu/v1/payments/";
        private const string cancelUrl = "/cancels";
        private const string BulkChargeUrl = ".dibspayment.eu/v1/subscriptions/charges";
        private const string TestUrlPrefix = "https://test.api";
        private const string ProductionUrlPrefix = "https://api";
        private const string OrderExistError = "orderreference already exist, please use an unique.";
        private string urlPrefix;
        private HttpClient client;

        private Monosoft.Service.Dibs.Database.Database database;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="serviceName">the name of this service.</param>

        public Logic(string serviceName)
        {
            var settings = ITUtil.Common.Config.GlobalRessources.getConfig();
            var sql = settings.GetSetting<ITUtil.Common.Config.SQLSettings>();


            this.database = new Monosoft.Service.Dibs.Database.Database(sql, serviceName.ToLower());

            this.GetDatabaseSettings();
        }

        /// <summary>
        /// Gets the private key to dibs.
        /// </summary>
        internal string SecretId { get; private set; }

        /// <summary>
        /// Gets the terms and conditions url for payments.
        /// </summary>
        internal string TermsUrl { get; private set; }

        /// <summary>
        /// Creates a subscription.
        /// </summary>
        /// <param name="subScriptionCreationInfo">Subscription needs an unique orderreference or it will fail.</param>
        /// <returns>returns string, incase of an error it will send it back.</returns>
        public PaymentSystem CreateSubscription(SubScriptionCreationInfo subScriptionCreationInfo)
        {
            var subscriptionRequest = this.PopulateSubscriptionRequest(subScriptionCreationInfo);

            string url = this.urlPrefix + PaymentUrl;

            var json = JsonConvert.SerializeObject(subscriptionRequest);
            var textConent = new StringContent(json, Encoding.UTF8, "application/json");

            var responseObject = this.client.PostAsync(url, textConent);
            var contents = responseObject.Result.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<PaymentSystem>(contents);

            if (this.database.AddPaymentInfo(result.paymentId, subScriptionCreationInfo.orderReference))
            {
            }
            else
            {
                throw new Exception(OrderExistError);
            }

            textConent.Dispose();

            return result;
        }

        public void RegisterSubscription(SubscriptionRegisterInfo reginfo)
        {
            if (this.database.AddPaymentInfo(reginfo.paymentId, reginfo.orderReference))
            {
            }
            else
            {
                throw new Exception(OrderExistError);
            }
        }

        public SuccessCancel CancelSubscription(DTO.SubScription.CancelSubscription cancelSubscription)
        {
            var responseObject = this.GetSubscriptionPaymentDetails(new OrderReference() { orderReference = cancelSubscription.orderReference });
            return this.CancelSubscription(new DTO.Payment.CancelRequest() { amount = cancelSubscription.amount, orderItems = cancelSubscription.orderItems, paymentId = responseObject.paymentId });
        }

        public SuccessCancel CancelSubscription(DTO.Payment.CancelRequest cancelRequest)
        {
            //contain data > items and amount
            //PaymentId
            //            {
            //                "amount":0,
            //   "orderItems":[
            //      {  
            //         "reference":"string",
            //         "name":"string",
            //         "quantity":0,
            //         "unit":"string",
            //         "unitPrice":0,
            //         "taxRate":0,
            //         "taxAmount":0,
            //         "grossTotalAmount":0,
            //         "netTotalAmount":0
            //      }
            //   ]
            //}
            var internalCancel = PopulateCancelRequest(cancelRequest);
            string url = this.urlPrefix + PaymentUrl + cancelRequest.paymentId + cancelUrl;
            var json = JsonConvert.SerializeObject(internalCancel);
            var textConent = new StringContent(json, Encoding.UTF8, "application/json");

            var responseObject = this.client.PostAsync(url, textConent);
            var contents = responseObject.Result.Content.ReadAsStringAsync().Result;
            var successCancel = new SuccessCancel()
            {
                didSucceed = contents == ""
            };
            return successCancel;

        }

        /// <summary>
        /// Creates a subscription.
        /// </summary>
        /// <param name="subScriptionCreationInfo">Subscription needs an unique orderreference or it will fail.</param>
        /// <returns>returns string, incase of an error it will send it back.</returns>
        public PaymentSystem CreatePayment(PaymentCreationInfo paymentCreationInfo)
        {
            var subscriptionRequest = this.PopulatePaymentRequest(paymentCreationInfo);

            string url = this.urlPrefix + PaymentUrl;

            var json = JsonConvert.SerializeObject(subscriptionRequest);
            var textConent = new StringContent(json, Encoding.UTF8, "application/json");

            var responseObject = this.client.PostAsync(url, textConent);
            var contents = responseObject.Result.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<PaymentSystem>(contents);

            if (this.database.AddPaymentInfo(result.paymentId, paymentCreationInfo.orderReference))
            {
            }
            else
            {
                throw new Exception(OrderExistError);
            }

            textConent.Dispose();

            return result;
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            this.client.Dispose();
        }

        /// <summary>
        /// Insert Settings into the database plus load the new settings into the system.
        /// </summary>
        /// <param name="settings">the new settings for Dibs.</param>
        public void SetSettings(Settings settings)
        {
            this.database.AddSettings(settings);
            this.GetDatabaseSettings();
        }

        /// <summary>
        /// Get details for payment by orderreference.
        /// </summary>
        /// <param name="orderReference">the unique id for payment.</param>
        /// <returns>Returns all information regarding payment.</returns>
        public Payment GetSubscriptionPaymentDetails(OrderReference orderReference)
        {
            string url = this.urlPrefix + PaymentUrl;

            var paymentInfo = this.database.GetPaymentInfo(orderReference.orderReference);
            PaymentDIBs paymentDibs;
            try
            {
                var responseObject = this.client.GetStringAsync(url + paymentInfo.paymentId).Result;
                paymentDibs = JsonConvert.DeserializeObject<PaymentDIBs>(responseObject);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return paymentDibs.payment;
        }

        /// <summary>
        /// Get details for payment by orderreference.
        /// </summary>
        /// <param name="orderReference">the unique id for payment.</param>
        /// <returns>Returns all information regarding payment.</returns>
        public PaymentSingle GetSinglePaymentDetails(OrderReference orderReference)
        {
            string url = this.urlPrefix + PaymentUrl;

            var paymentInfo = this.database.GetPaymentInfo(orderReference.orderReference);

            var responseObject = this.client.GetStringAsync(url + paymentInfo.paymentId).Result;
            var paymentDibs = JsonConvert.DeserializeObject<PaymentDIBs>(responseObject);
            return paymentDibs.payment;
        }

        /// <summary>
        /// Gets the id for subscription used in bulkcharge.
        /// </summary>
        /// <param name="orderReference">the unique id for subscription.</param>
        /// <returns>returns subscription id.</returns>
        public DTO.Subscription GetSubscriptionId(OrderReference orderReference)
        {
            var responseObject = this.GetSubscriptionPaymentDetails(orderReference);
            return responseObject.subscription;
        }

        /// <summary>
        /// Call to make a request to dibs for charging multiple subscriptions.
        /// </summary>
        /// <param name="subBulkCharge">contains all the client infomration needed to make a bulk charge.</param>
        /// <returns>Return the unique id for bulkcharge, so you can check the status for subscriptions later.</returns>
        public BulkInfo BulkCharge(BulkChargeCreationInfo subBulkCharge)
        {
            string url = this.urlPrefix + BulkChargeUrl;

            var subscriptions = new List<SubscriptionSystem>(subBulkCharge.subScriptions.Length);
            for (int cnt = 0; cnt < subBulkCharge.subScriptions.Length; cnt++)
            {
                int amount;
                var items = CloneItems(subBulkCharge.subScriptions[cnt].Items, out amount);
                subscriptions.Add(
                new SubscriptionSystem
                {
                    order = new Order()
                    {
                        amount = amount,
                        currency = subBulkCharge.subScriptions[cnt].Order.currency,
                        items = items,
                        reference = subBulkCharge.subScriptions[cnt].Order.reference,
                    },
                    subscriptionId = subBulkCharge.subScriptions[cnt].SubscriptionId,
                });
            }

            var subscriptionRequest = new BulkCharge()
            {
                externalBulkChargeId = subBulkCharge.externalBulkChargeId,
                subScriptions = subscriptions.ToArray(),
                notifications = null,
            };

            var json = JsonConvert.SerializeObject(subscriptionRequest);
            var textConent = new StringContent(json, Encoding.UTF8, "application/json");

            var responseObject = this.client.PostAsync(url, textConent);

            var contents = responseObject.Result.Content.ReadAsStringAsync().Result;

            var result = JsonConvert.DeserializeObject<BulkSystem>(contents);

            var bulkInfo = new BulkInfo(subBulkCharge.externalBulkChargeId, result.bulkId, subBulkCharge.subScriptions.Length);
            this.database.AddBulkInfo(bulkInfo.externalBulkChargeId, bulkInfo.bulkChargeId, bulkInfo.subScriptionAmount);

            textConent.Dispose();

            return bulkInfo;
        }

        public ChargeInfo SinglePaymentCharge(SingleChargeCreationInfo singleChargeCreationInfo)
        {
            string url = this.urlPrefix + PaymentUrl;
            var paymentInfo = this.database.GetPaymentInfo(singleChargeCreationInfo.orderReference);
            url += paymentInfo.paymentId + "/charges";
            var items = CloneItems(singleChargeCreationInfo.orderItems, out int amount);
            var singleCharge = new SingleCharge(amount, items);
            var json = JsonConvert.SerializeObject(singleCharge);
            var textConent = new StringContent(json, Encoding.UTF8, "application/json");

            var responseObject = this.client.PostAsync(url, textConent);

            var contents = responseObject.Result.Content.ReadAsStringAsync().Result;

            var result = JsonConvert.DeserializeObject<ChargeInfo>(contents);

            //var bulkInfo = new BulkInfo(subBulkCharge.externalBulkChargeId, result.bulkId, subBulkCharge.subScriptions.Length);
            //this.database.AddBulkInfo(bulkInfo.externalBulkChargeId, bulkInfo.bulkChargeId, bulkInfo.subScriptionAmount);

            textConent.Dispose();

            return result;
        }

        /// <summary>
        /// Clone items from client request to internal DTO items.
        /// </summary>
        /// <param name="items">the list of items.</param>
        /// <param name="amount">the total amount from grossTotal per item.</param>
        /// <returns>returns internal items list for making dibs request.</returns>
        private static DTO.Internal.Item[] CloneItems(DTO.Item[] items, out int amount)
        {
            amount = 0;
            var internalItems = new DTO.Internal.Item[items.Length];

            for (int cnt = 0; cnt < items.Length; cnt++)
            {
                internalItems[cnt].name = items[cnt].Name;
                internalItems[cnt].netTotalAmount = items[cnt].NetTotalAmount;
                internalItems[cnt].taxRate = items[cnt].TaxRate;
                internalItems[cnt].taxAmount = items[cnt].NetTotalAmount * items[cnt].TaxRate / 10000;
                internalItems[cnt].grossTotalAmount = items[cnt].NetTotalAmount + internalItems[cnt].taxAmount;
                internalItems[cnt].reference = items[cnt].Reference;
                internalItems[cnt].unit = items[cnt].Unit;
                internalItems[cnt].unitPrice = items[cnt].UnitPrice;
                internalItems[cnt].quantity = items[cnt].Quantity;
                amount += internalItems[cnt].grossTotalAmount;
            }

            return internalItems;
        }

        /// <summary>
        /// Create a internal subscription request from a client subscriptionRequest.
        /// </summary>
        /// <param name="subScriptionCreationInfo">contains all the client information.</param>
        /// <returns>returns the internal subscruptionRequest.</returns>
        private SubScriptionRequest PopulateSubscriptionRequest(SubScriptionCreationInfo subScriptionCreationInfo)
        {
            var subScriptionRequest = default(SubScriptionRequest);

            subScriptionRequest.order = new Order()
            {
                items = CloneItems(subScriptionCreationInfo.items, out int amount),
                amount = amount,
                currency = subScriptionCreationInfo.currency,
                reference = subScriptionCreationInfo.orderReference,
            };

            subScriptionRequest.checkout = new DTO.Internal.Checkout()
            {
                merchantHandlesConsumerData = true,
                url = subScriptionCreationInfo.url,
                termsUrl = this.TermsUrl,
                charge = subScriptionCreationInfo.immediateCharge,
                consumerType = subScriptionCreationInfo.consumerType,
                shipping = new Shipping()
                {
                    countries = subScriptionCreationInfo.shippingCountries,
                    merchantHandlesShippingCost = subScriptionCreationInfo.merchantHandlesShippingCost,
                },
                shippingCountries = null,
            };

            subScriptionRequest.subscription = new DTO.Internal.Subscription()
            {
                interval = subScriptionCreationInfo.interval,
                endDate = subScriptionCreationInfo.endDate,
            };

            subScriptionRequest.notifications = subScriptionCreationInfo.notifications;

            return subScriptionRequest;
        }

        /// <summary>
        /// Create a internal subscription request from a client subscriptionRequest.
        /// </summary>
        /// <param name="paymentCreationInfo">contains all the client information.</param>
        /// <returns>returns the internal subscruptionRequest.</returns>
        private DTO.Internal.Payment.CancelRequest PopulateCancelRequest(DTO.Payment.CancelRequest internalCancelRequest)
        {
            var cancelRequest = default(DTO.Internal.Payment.CancelRequest);

            cancelRequest.amount = internalCancelRequest.amount;
            cancelRequest.orderItems = CloneItems(internalCancelRequest.orderItems, out int amountCloned);

            return cancelRequest;
        }

        /// <summary>
        /// Create a internal subscription request from a client subscriptionRequest.
        /// </summary>
        /// <param name="paymentCreationInfo">contains all the client information.</param>
        /// <returns>returns the internal subscruptionRequest.</returns>
        private PaymentRequest PopulatePaymentRequest(PaymentCreationInfo paymentCreationInfo)
        {
            var subScriptionRequest = default(PaymentRequest);

            subScriptionRequest.order = new Order()
            {
                items = CloneItems(paymentCreationInfo.items, out int amount),
                amount = amount,
                currency = paymentCreationInfo.currency,
                reference = paymentCreationInfo.orderReference,
            };

            subScriptionRequest.checkout = new DTO.Internal.Checkout()
            {
                url = paymentCreationInfo.url,
                termsUrl = this.TermsUrl,
                consumerType = paymentCreationInfo.consumerType,
                shipping = new Shipping()
                {
                    countries = paymentCreationInfo.shippingCountries,
                    merchantHandlesShippingCost = paymentCreationInfo.merchantHandlesShippingCost,
                },
                shippingCountries = null,
            };

            return subScriptionRequest;
        }

        private void CreateHttpClient()
        {
            this.client = new HttpClient();
            this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(this.SecretId);
            this.client.DefaultRequestHeaders.Accept.TryParseAdd("application/json");
        }

        /// <summary>
        /// Gets the settings from database.
        /// </summary>
        private void GetDatabaseSettings()
        {
            var dibsSettings = this.database.GetSettings();
            if (dibsSettings != null)
            {
                if (dibsSettings.isTesting)
                {
                    this.urlPrefix = TestUrlPrefix;
                }
                else
                {
                    this.urlPrefix = ProductionUrlPrefix;
                }

                this.SecretId = dibsSettings.secretKey;
                this.TermsUrl = dibsSettings.termsUrl;
                this.CreateHttpClient();
            }
        }

        public BulkDetails GetBulkDetailsFull(BulkExternalId bulkExternalId)
        {
            var bulkInfo = this.database.GetBulkInfo(bulkExternalId.id);
            string baseUrl = this.urlPrefix + BulkChargeUrl;
            string totalUrl = baseUrl + "/" + bulkInfo.bulkChargeId;// + "?pagenumber=" + 1 + "&pagesize=" + bulkInfo.subScriptionAmount;
            var contents = this.client.GetStringAsync(totalUrl).Result;
            var result = JsonConvert.DeserializeObject<BulkDetails>(contents);
            return result;
        }

        public BulkDetails GetBulkDetailsPage(BulkPageInfo bulkPage)
        {
            var bulkInfo = this.database.GetBulkInfo(bulkPage.externalId);
            string baseUrl = this.urlPrefix + BulkChargeUrl;
            string totalUrl = baseUrl + "/" + bulkInfo.bulkChargeId + "?pagenumber=" + bulkPage.pageNumber + "&pagesize=" + bulkPage.pageSize;
            var contents = this.client.GetStringAsync(totalUrl).Result;
            var result = JsonConvert.DeserializeObject<BulkDetails>(contents);
            return result;
        }

        // internal string GetSubScriptionDetails(OrderReference orderReference)
        // {
        //    //string url = this.urlPrefix + PaymentUrl;
        //    this.client = new HttpClient();

        // //this.client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(this.SecretId);
        // //this.client.DefaultRequestHeaders.Accept.TryParseAdd("application/json");

        // var responseObject = this.GetPaymentDetails(orderReference); //= this.client.GetStringAsync(url + "/" + paymentSystem.paymentId).Result;
        // var payment = JsonConvert.DeserializeObject<PaymentDIBs>(responseObject);

        // string url = this.urlPrefix + SubscriptionUrl;

        // string subscriptionResponse = this.client.GetStringAsync(url + payment.payment.subscription.id).Result;

        // return subscriptionResponse;
        // }
    }
}
