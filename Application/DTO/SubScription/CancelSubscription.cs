﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.Dibs.DTO.SubScription
{
    public class CancelSubscription
    {
        public string orderReference { get; set; }

        public int amount { get; set; }

        public Item[] orderItems { get; set; }
    }
}
