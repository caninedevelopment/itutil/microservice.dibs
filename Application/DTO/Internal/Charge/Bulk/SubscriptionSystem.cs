﻿// <copyright file="SubscriptionSystem.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO.Internal
{
    /// <summary>
    /// contains all the subscriptions used for bulkCharge.
    /// </summary>
    internal struct SubscriptionSystem
    {
        /// <summary>
        /// Gets or sets the order for this charge.
        /// </summary>
        public DTO.Internal.Order order { get; set; }

        /// <summary>
        /// Gets or sets the unique id for subscription to sent to Dibs.
        /// </summary>
        public string subscriptionId { get; set; }
    }
}
