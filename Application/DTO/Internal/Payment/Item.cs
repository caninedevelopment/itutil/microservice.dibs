﻿// <copyright file="Item.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO.Internal
{
    /// <summary>
    /// Contain all informtation for each item to pay for that is being sent to dibs.
    /// </summary>
    public struct Item
    {
        /// <summary>
        /// Gets or sets the reference to each item.
        /// </summary>
        public string reference { get; set; }

        /// <summary>
        /// Gets or sets name pr item.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets quanitity pr item.
        /// </summary>
        public int quantity { get; set; }

        /// <summary>
        /// Gets or sets the unit type pr item.
        /// </summary>
        public string unit { get; set; }

        /// <summary>
        /// Gets or sets unitPrice pr item.
        /// </summary>
        public int unitPrice { get; set; }

        /// <summary>
        /// Gets or sets taxrate pr item.
        /// </summary>
        public int taxRate { get; set; }

        /// <summary>
        /// Gets or sets taxAmount taxamount for each item.
        /// </summary>
        public int taxAmount { get; set; }

        /// <summary>
        /// Gets or sets the total price for an item.
        /// </summary>
        public int grossTotalAmount { get; set; }

        /// <summary>
        /// Gets or sets the total price minus taxes.
        /// </summary>
        public int netTotalAmount { get; set; }
    }
}
