﻿// <copyright file="Checkout.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO.Internal
{
    /// <summary>
    /// Contains all informtaions sent to Dibs for checkout creation.
    /// </summary>
    public struct Checkout
    {
        /// <summary>
        /// Gets or sets the checkout url.
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// Gets or sets Terms and condition url for the payment.
        /// </summary>
        public string termsUrl { get; set; }

        /// <summary>
        /// Gets or sets the inclusive list of countries to ship to. if none is provided all countries are elgible.
        /// </summary>
        public object shippingCountries { get; set; }

        /// <summary>
        ///  Gets or sets the inclusive list of countries to ship to. if none is provided all countries are elgible.
        /// </summary>
        public Shipping shipping { get; set; }

        /// <summary>
        /// Gets or sets the consumer type.
        /// </summary>
        public string[] consumerType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are going to make an immediately charge to the costumer.
        /// </summary>
        public bool charge { get; set; }

        public bool merchantHandlesConsumerData { get; set; }
    }
}
