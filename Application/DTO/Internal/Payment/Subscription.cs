﻿// <copyright file="Shipping.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
using System;

namespace Monosoft.Service.Dibs.DTO.Internal
{
    /// <summary>
    /// Contains how many times we charge plus how when each subscritions duration is.
    /// </summary>
    public struct Subscription
    {
        /// <summary>
        /// Gets or sets the endate for a subscription.
        /// </summary>
        public DateTime endDate { get; set; }

        /// <summary>
        /// Gets or sets the interval we can charge a costumer (0= we control it).
        /// </summary>
        public int interval { get; set; }
    }
}
