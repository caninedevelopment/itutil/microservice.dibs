﻿// <copyright file="Summary.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO
{
    /// <summary>
    /// Contains summary of order.
    /// </summary>
    public struct Summary
    {
        /// <summary>
        /// Gets or sets the reserved amount in order.
        /// </summary>
        public string reservedAmount { get; set; }
        /// <summary>
        /// Gets or sets the cancelled amount in order.
        /// </summary>
        public string cancelledAmount { get; set; }

        /// <summary>
        /// Gets or sets the refunded amount in order.
        /// </summary>
        public string refundedAmount { get; set; }

        /// <summary>
        /// Gets or sets the charged amount in order.
        /// </summary>
        public string chargedAmount { get; set; }

    }
}