﻿// <copyright file="Consumer.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO
{
    public struct Consumer
    {
        /// <summary>
        /// Gets or sets shipping adress from Dibs.
        /// </summary>
        public ShippingAddress shippingAddress { get; set; }

        /// <summary>
        /// Gets or sets company information from Dibs.
        /// </summary>
        public Company company { get; set; }

        /// <summary>
        /// Gets or sets private person sent from Dibs.
        /// </summary>
        public PrivatePerson privatePerson { get; set; }
    }
}