﻿// <copyright file="Payment.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO.Payment
{
    /// <summary>
    /// Contains all payment Details from Dibs.
    /// </summary>
    public class Payment
    {
        /// <summary>
        /// Gets or sets the unique paymentId from Dibs.
        /// </summary>
        public string paymentId { get; set; }

        ///// <summary>
        ///// Gets or sets Summary from Dibs.
        ///// </summary>
        //public Summary summary { get; set; }

        /// <summary>
        /// Gets or sets Consumer information from Dibs.
        /// </summary>
        public Consumer consumer { get; set; }

        /// <summary>
        /// Gets or sets paymentDetails from Dibs.
        /// </summary>
        public PaymentDetails paymentDetails { get; set; }

        /// <summary>
        /// Gets or sets orderdetails from dibs.
        /// </summary>
        public OrderDetails orderDetails { get; set; }

        /// <summary>
        /// Gets or sets Checkout information returned by dibs.
        /// </summary>
        public Checkout checkout { get; set; }

        /// <summary>
        /// Gets or sets when payment is created.
        /// </summary>
        public string created { get; set; }

        /// <summary>
        /// Gets or sets subscription id from Dibs.
        /// </summary>
        public Subscription subscription { get; set; }
    }
}