﻿// <copyright file="ShippingAddress.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO
{
    /// <summary>
    /// Contains shipping adress incase the product needs to be sent.
    /// </summary>
    public struct ShippingAddress
    {
        /// <summary>
        /// Gets or sets first line in adress.
        /// </summary>
        public string addressLine1 { get; set; }

        /// <summary>
        /// Gets or sets second line in adress.
        /// </summary>
        public string addressLine2 { get; set; }

        /// <summary>
        /// Gets or sets receiverLine.
        /// </summary>
        public string receiverLine { get; set; }

        /// <summary>
        /// Gets or sets postal code.
        /// </summary>
        public string postalCode { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// Gets or sets Country.
        /// </summary>
        public string country { get; set; }
    }
}