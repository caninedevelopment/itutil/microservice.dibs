﻿// <copyright file="item.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO
{
    public class Item
    {
        /// <summary>
        /// Gets or sets item reference.
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// Gets or sets the name of item returned from Dibs.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the quantity of items returned from Dibs.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the unit type pr Item.
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets Unit price per item.
        /// </summary>
        public int UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets taxRate for each item returned from Dibs.
        /// OBS! This value is in promille eg. 25% => 2500
        /// </summary>
        public int TaxRate { get; set; }

        /// <summary>
        /// Gets or sets the total price minus taxes.
        /// </summary>
        public int NetTotalAmount { get; set; }
    }
}