﻿// <copyright file="CardDetails.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO
{
    /// <summary>
    /// Contains the card details we are sending back to client.
    /// </summary>
    public struct CardDetails
    {
        /// <summary>
        /// Gets or sets the masked card information.
        /// </summary>
        public string maskedDetails { get; set; }

        /// <summary>
        /// Gets or sets expiryDate for a card.
        /// </summary>
        public string expiryDate { get; set; }
    }
}