﻿// <copyright file="SubScriptionCreationInfo.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO
{

    /// <summary>
    /// is template transfering of data to creat a subscrition.
    /// To read more go to https://tech.dibspayment.com/easy/integration/recurring#createSub .
    /// </summary>
    public class PaymentCreationInfo
    {
        /// <summary>
        /// Gets or sets currencyType. by iso-code4217 > https://en.wikipedia.org/wiki/ISO_4217#Active_codes .
        /// </summary>
        public string currency { get; set; }

        /// <summary>
        /// Gets or sets the reference for this total order (max 27 characters).
        /// </summary>
        public string orderReference { get; set; }

        /// <summary>
        /// Gets or sets the checkout Url. WIll be shown and 3-d secure verification. https://tech.dibspayment.com/easy/api/datastring-parameters .
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// Gets or sets the countries aviable to ship to (this is list is inclusive). get the ISO https://tech.dibspayment.com/easy/api/datastring-parameters .
        /// </summary>
        public Country[] shippingCountries { get; set; }

        /// <summary>
        /// Gets or sets if b2b, b2c or both.
        /// </summary>
        public string[] consumerType { get; set; }


        /// <summary>
        /// Gets or sets PartnerID.
        /// </summary>
        public string merchantNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the merchant or the user handles shipping costs.
        /// </summary>
        public bool merchantHandlesShippingCost { get; set; }

        /// <summary>
        /// Gets or sets list of all items to order.
        /// </summary>
        public Item[] items { get; set; }
    }
}
