﻿// <copyright file="OrderDetails.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.DTO
{
    /// <summary>
    /// Contains the Orderdetails sent back from Dibs.
    /// </summary>
    public class OrderDetails
    {
        /// <summary>
        /// Gets or sets total amount for order.
        /// </summary>
        public int amount { get; set; }

        /// <summary>
        /// Gets or sets the ISO code for currency.
        /// </summary>
        public string currency { get; set; }

        /// <summary>
        /// Gets or sets the unique Order reference.
        /// </summary>
        public string reference { get; set; }
    }
}