﻿// <copyright file="BulkInfo.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.Database
{
    using ServiceStack.DataAnnotations;

    /// <summary>
    /// Contains all microservice settings.
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Gets or sets secret key from Dibs.
        /// </summary>
        [Unique]
        public string secretKey { get; set; }

        /// <summary>
        /// Gets or sets terms url.
        /// </summary>
        public string termsUrl { get; set; }

        /// <summary>
        /// Gets or sets wether we are in testing mode.
        /// </summary>
        public bool isTesting { get; set; }

        public Settings()
        {
        }
            /// <summary>
            /// Initializes a new instance of the <see cref="Settings"/> class.
            /// </summary>
            /// <param name="isTesting">set to true if is in testing.</param>
            /// <param name="secretKey">Scret key from dibs.</param>
            /// <param name="termsUrl">Terms and condition url from client.</param>
            public Settings(bool isTesting, string secretKey, string termsUrl)
        {
            this.isTesting = isTesting;
            this.secretKey = secretKey;
            this.termsUrl = termsUrl;
        }
    }
}
