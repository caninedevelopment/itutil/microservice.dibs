﻿// <copyright file="Database.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.Database
{
    using ServiceStack.OrmLite;
    using static ITUtil.Common.Config.SQLSettings;

    /// <summary>
    /// Contains all behvaiour and states for the database.
    /// </summary>
    public class Database
    {
        private static OrmLiteConnectionFactory dbFactory = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="Database"/> class.
        /// </summary>
        /// <param name="settings">SQL settings to use for starting database.</param>
        /// <param name="name">name for the database.</param>
        public Database(ITUtil.Common.Config.SQLSettings settings, string name)
        {
            if (settings == null)
            {
                throw new System.ArgumentNullException(nameof(settings));
            }

            OrmLiteConnectionFactory masterDb = GetConnectionFactory(settings);
            var dbName = name;
            using (var db = masterDb.Open())
            {
                settings.CreateDatabaseIfNoExists(db, dbName);
            }

            // set the factory up to use the microservice database
            dbFactory = GetConnectionFactory(settings, name);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<PaymentInfo>();
                db.CreateTableIfNotExists<Settings>();
                db.CreateTableIfNotExists<BulkInfo>();
            }
        }

        /// <summary>
        /// ClearDatabase.
        /// </summary>
        public void ClearDatabase()
        {
            using (var db = dbFactory.Open())
            {
                db.DeleteAll<PaymentInfo>();
                db.DeleteAll<Settings>();
                db.DeleteAll<BulkInfo>();
            }
        }

        /// <summary>
        /// Stores the payment Info needed to get payment details later.
        /// </summary>
        /// <param name="paymentId">payment id from dibs.</param>
        /// <param name="orderReference">the unique id created by client.</param>
        /// <returns>if it already exist.</returns>
        public bool AddPaymentInfo(string paymentId, string orderReference)
        {
            using (var db = dbFactory.Open())
            {
                var dbResult = db.Select<PaymentInfo>(x => x.orderReference == orderReference);
                if (dbResult.Count == 0)
                {
                    db.Insert<PaymentInfo>(new PaymentInfo(paymentId, orderReference));
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Get payment ID from orderreference.
        /// </summary>
        /// <param name="orderReference">the unique id created by client.</param>
        /// <returns>returns payment info or null if none exist.</returns>
        public PaymentInfo GetPaymentInfo(string orderReference)
        {
            using (var db = dbFactory.Open())
            {
                var task = db.Select<PaymentInfo>(x => x.orderReference == orderReference);
                if (task.Count > 0)
                {
                    return task[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Save the bulk information to the database.
        /// </summary>
        /// <param name="externalBulkChargeId">unique external bulkCharge id.</param>
        /// <param name="bulkChargeId">unique bulkd id made by dibs.</param>
        /// <param name="subScriptionAmount">The amount of subscriptions in bulk charge.</param>
        /// <returns>returns true if saved to database, returns false if it already exist.</returns>
        public bool AddBulkInfo(string externalBulkChargeId, string bulkChargeId, int subScriptionAmount)
        {
            using (var db = dbFactory.Open())
            {
                var dbResult = db.SingleById<BulkInfo>(externalBulkChargeId);
                if (dbResult == null)
                {
                    db.Insert<BulkInfo>(new BulkInfo(externalBulkChargeId, bulkChargeId, subScriptionAmount));
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the Bulk info from database.
        /// </summary>
        /// <param name="externalBulkChargeId">the unique external bulkcharge id.</param>
        /// <returns>returns bulkInfo if it exist else null.</returns>
        public BulkInfo GetBulkInfo(string externalBulkChargeId)
        {
            using (var db = dbFactory.Open())
            {
                var task = db.Select<BulkInfo>(x => x.externalBulkChargeId == externalBulkChargeId);
                if (task.Count > 0)
                {
                    return task[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// adds setings if none exists.
        /// </summary>
        /// <param name="settings"> the settings you want to add.</param>
        /// <returns> returns if it worked.</returns>
        public bool AddSettings(Settings settings)
        {
            using (var db = dbFactory.Open())
            {
                var dbResult = db.Select<Settings>();
                if (dbResult.Count == 0)
                {
                    db.Insert<Settings>(settings);
                    return true;
                }
                else
                {
                    db.DeleteAll<Settings>();
                    db.Insert<Settings>(settings);
                    return false;
                }
            }
        }

        /// <summary>
        /// Update SecretKey to Dibs.
        /// </summary>
        /// <param name="secretKey">new ScretKey.</param>
        /// <returns> returns if it worked.</returns>
        public bool UpdateSecretKey(string secretKey)
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.Select<Settings>();
                if (dbresult.Count > 0)
                {
                    dbresult[0].secretKey = secretKey;
                    db.Update<Settings>(dbresult);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Update testing state.
        /// </summary>
        /// <param name="isTesting">sets the new testingState.</param>
        /// <returns> returns if it worked.</returns>
        public bool UpdateTesting(bool isTesting)
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.Select<Settings>();
                if (dbresult.Count > 0)
                {
                    dbresult[0].isTesting = isTesting;
                    db.Update<Settings>(dbresult);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets the settings from the database.
        /// </summary>
        /// <returns>returns if contain setting else null.</returns>
        public Settings GetSettings()
        {
            using (var db = dbFactory.Open())
            {
                var task = db.Select<Settings>();
                if (task.Count > 0)
                {
                    return task[0];
                }
                else
                {
                    return null;
                }
            }
        }

        private static OrmLiteConnectionFactory GetConnectionFactory(ITUtil.Common.Config.SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServerDialect.Provider);
                case SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }
    }
}
