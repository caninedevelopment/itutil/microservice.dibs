﻿// <copyright file="BulkInfo.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.Database
{
    using ServiceStack.DataAnnotations;

    /// <summary>
    /// Contains the payment information to store in database about payment.
    /// </summary>
    public class PaymentInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentInfo"/> class.
        /// </summary>
        /// <param name="paymentId">Unique paymentID from Dibs.</param>
        /// <param name="orderReference">Unique order reference from client.</param>
        public PaymentInfo(string paymentId, string orderReference)
        {
            this.paymentId = paymentId;
            this.orderReference = orderReference;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentInfo"/> class.
        /// </summary>
        public PaymentInfo()
        {
        }

        /// <summary>
        /// Gets or sets the unique paymentId from Dibs.
        /// </summary>
        [Unique]
        public string paymentId { get; set; }

        /// <summary>
        /// Gets or sets the unique orderreference from client.
        /// </summary>
        [Unique]
        public string orderReference { get; set; }
    }
}
