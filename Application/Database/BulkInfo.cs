﻿// <copyright file="BulkInfo.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs.Database
{
    using ServiceStack.DataAnnotations;

    /// <summary>
    /// Contains the information for the database.
    /// </summary>
    public class BulkInfo
    {
        /// <summary>
        /// Gets or sets the unique bulkChargeId from Dibs.
        /// </summary>
        [Unique]
        public string bulkChargeId { get; set; }

        /// <summary>
        /// Gets or sets the unique externalBulkChargeId from client.
        /// </summary>
        [Unique]
        public string externalBulkChargeId { get; set; }

        public int subScriptionAmount { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BulkInfo"/> struct.
        /// </summary>
        /// <param name="externalBulkChargeId">The unique external bulk Charge id from client.</param>
        /// <param name="bulkChargeId">the unique bulk charge id from Dibs.</param>
        public BulkInfo(string externalBulkChargeId, string bulkChargeId, int subScriptionAmount)
        {
            this.bulkChargeId = bulkChargeId;
            this.externalBulkChargeId = externalBulkChargeId;
            this.subScriptionAmount = subScriptionAmount;
        }
    }
}
