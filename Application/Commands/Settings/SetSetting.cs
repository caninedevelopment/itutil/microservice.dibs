﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.Database;

namespace Monosoft.Service.Dibs.Commands.Setting
{
    public class SetSetting : UpdateCommand<Settings>
    {
        public SetSetting() : base("Set the settings for this micro service, dibs.")
        { }

        public override void Execute(Settings input)
        {
            Namespace.logic.SetSettings(input);
        }
    }
}