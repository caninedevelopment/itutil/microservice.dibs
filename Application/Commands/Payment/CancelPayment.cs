﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Charge.Payment;
using Monosoft.Service.Dibs.DTO.Payment;

namespace Monosoft.Service.Dibs.Commands.Payment
{
    public class CancelPayment : GetCommand<CancelRequest, SuccessCancel>
    {
        public CancelPayment() : base("Cancel existing payment by paymentId", OperationType.Delete)
        { }

        public override SuccessCancel Execute(CancelRequest input)
        {
            return Namespace.logic.CancelSubscription(input);
        }
    }
}