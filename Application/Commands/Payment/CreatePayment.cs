﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO;

namespace Monosoft.Service.Dibs.Commands.Payment
{
    public class CreatePayment : GetCommand<PaymentCreationInfo, PaymentSystem>
    {
        public CreatePayment() : base("Creates the payment to use", OperationType.Insert)
        { }

        public override PaymentSystem Execute(PaymentCreationInfo input)
        {
            return Namespace.logic.CreatePayment(input);
        }
    }
}
