﻿// <copyright file="Controller.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Dibs.Commands.Payment
{
    using System.Collections.Generic;
    using ITUtil.Common.Command;

    /// <summary>
    /// Controller contains all methods to bed called from outside the microservice.
    /// </summary>
    public class Namespace : BaseNamespace
    {
        /// <summary>
        /// contain the logic variable.
        /// </summary>
        public static Logic logic;

        public Namespace() : base("Dibs_Payment", new ITUtil.Common.Command.ProgramVersion("1.0.0.0"))
        {
            logic = new Logic("dibs");
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new ChargePayment(),
                new CreatePayment(),
                new CancelPayment(),
                new GetPaymentDetails()
            });
        }
    }
}