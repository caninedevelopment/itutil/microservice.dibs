﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Payment;

namespace Monosoft.Service.Dibs.Commands.Payment
{
    public class GetPaymentDetails : GetCommand<OrderReference, Monosoft.Service.Dibs.DTO.Payment.Payment>
    {
        public GetPaymentDetails() : base("is charge all the subscriptions sent to the bulk")
        { }
        public override Monosoft.Service.Dibs.DTO.Payment.Payment Execute(OrderReference input)
        {
            return Namespace.logic.GetSinglePaymentDetails(input);
        }
    }
}