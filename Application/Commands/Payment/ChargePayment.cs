﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Charge.Payment;
using Monosoft.Service.Dibs.DTO.Payment;

namespace Monosoft.Service.Dibs.Commands.Payment
{
    public class ChargePayment : GetCommand<SingleChargeCreationInfo, ChargeInfo>
    {
        public ChargePayment() : base("is charge all the subscriptions sent to the bulk", OperationType.Insert)
        { }

        public override ChargeInfo Execute(SingleChargeCreationInfo input)
        {
            return Namespace.logic.SinglePaymentCharge(input);
        }
    }
}