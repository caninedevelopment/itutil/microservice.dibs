﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Payment;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class GetSubscriptionDetails : GetCommand<OrderReference, Monosoft.Service.Dibs.DTO.Payment.Payment>
    {
        public GetSubscriptionDetails() : base("is charge all the subscriptions sent to the bulk")
        { }
        public override Monosoft.Service.Dibs.DTO.Payment.Payment Execute(OrderReference input)
        {
            return Namespace.logic.GetSubscriptionPaymentDetails(input);
        }
    }
}