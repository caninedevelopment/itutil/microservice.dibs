﻿namespace Monosoft.Service.Dibs.Commands.Subscription
{
    using global::Monosoft.Service.Dibs.DTO;
    using ITUtil.Common.Command;

    public class Register : InsertCommand<SubscriptionRegisterInfo>
    {
        public Register() : base("Register a known payment-orderreference relation", OperationType.Insert)
        { }
        public override void Execute(SubscriptionRegisterInfo input)
        {
            Namespace.logic.RegisterSubscription(input);
        }
    }
}