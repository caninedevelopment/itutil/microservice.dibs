﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Charge.Payment;
using Monosoft.Service.Dibs.DTO.Payment;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class CancelSubscription : GetCommand<CancelRequest, SuccessCancel>
    {
        public CancelSubscription() : base("Cancel existing subscription by paymentId", OperationType.Delete)
        { }

        public override SuccessCancel Execute(CancelRequest input)
        {
            return Namespace.logic.CancelSubscription(input);
        }
    }
}