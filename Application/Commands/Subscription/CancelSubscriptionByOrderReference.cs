﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Charge.Payment;
using Monosoft.Service.Dibs.DTO.Payment;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class CancelSubscriptionByOrderReference : GetCommand<DTO.SubScription.CancelSubscription, SuccessCancel>
    {
        public CancelSubscriptionByOrderReference() : base("Cancel existing subscription by orderReference", OperationType.Delete)
        { }

        public override SuccessCancel Execute(DTO.SubScription.CancelSubscription input)
        {
            return Namespace.logic.CancelSubscription(input);
        }
    }
}