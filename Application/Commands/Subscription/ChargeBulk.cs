﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.Database;//SUK, lange!!!
using Monosoft.Service.Dibs.DTO;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class ChargeBulk : GetCommand<BulkChargeCreationInfo, BulkInfo>
    {
        public ChargeBulk() : base("is charge all the subscriptions sent to the bulk", OperationType.Insert)
        { }
        public override BulkInfo Execute(BulkChargeCreationInfo input)
        {
            return Namespace.logic.BulkCharge(input);
        }
    }
}
