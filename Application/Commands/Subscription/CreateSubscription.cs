﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class CreateSubscription : GetCommand<SubScriptionCreationInfo, PaymentSystem>
    {
        public CreateSubscription() : base("Creates the subscription to use", OperationType.Insert)
        { }
        public override PaymentSystem Execute(SubScriptionCreationInfo input)
        {
            return Namespace.logic.CreateSubscription(input);
        }
    }
}
