﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Charge.Bulk;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class GetBulkPage : GetCommand<BulkPageInfo, BulkDetails>
    {
        public GetBulkPage() : base("is charge all the subscriptions sent to the bulk")
        { }
        public override BulkDetails Execute(BulkPageInfo input)
        {
            return Namespace.logic.GetBulkDetailsPage(input);
        }
    }
}