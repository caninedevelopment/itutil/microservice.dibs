﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Payment;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class GetSubscriptionId : GetCommand<OrderReference, DTO.Subscription>
    {
        public GetSubscriptionId() : base("Get subscription id to use in bulkcharge")
        { }
        public override DTO.Subscription Execute(OrderReference input)
        {
            return Namespace.logic.GetSubscriptionId(input);
        }
    }
}
