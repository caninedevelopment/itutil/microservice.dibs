﻿using ITUtil.Common.Command;
using Monosoft.Service.Dibs.DTO.Charge.Bulk;

namespace Monosoft.Service.Dibs.Commands.Subscription
{
    public class GetBulkDetails : GetCommand<BulkExternalId, BulkDetails>
    {
        public GetBulkDetails() : base("is charge all the subscriptions sent to the bulk")
        { }
        public override BulkDetails Execute(BulkExternalId input)
        {
            return Namespace.logic.GetBulkDetailsFull(input);
        }
    }
}