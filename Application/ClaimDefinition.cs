﻿// <copyright file="ClaimDefinition.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace Monosoft.Service.Dibs
{
    using System.Collections.Generic;

//    /// <summary>
//    /// ClaimDefinition cotain the metadata for the microservice.
//    /// </summary>
//    public static class ClaimDefinition
//    {
//        /// <summary>
//        /// Set to have the correct security clearence for access to data.
//        /// </summary>
//        internal static readonly MetaDataDefinition IsAdmin = new MetaDataDefinition(
//         "dibs",
//         "isAdmin",
//         MetaDataDefinition.DataContextEnum.organisationClaims,
//         new LocalizedString("en", "User is adminstrator, so they can charge and create subscriptions/one time payments towards the correct user"));

//        /// <summary>
//        /// Contains all claims.
//        /// </summary>
//        internal static readonly MetaDataDefinitions Definitions = new MetaDataDefinitions( //Make me to an array declartions .
//       new List<MetaDataDefinition>()
//                           {
//                                //ClaimDefinition.IsAdmin,
//                           }.ToArray());
//    }
}
